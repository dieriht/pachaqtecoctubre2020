Cesar Gonzales

Clase 1 (Sesion 1 Semana 1)

# BACK END

  

## Un desarrollador Front-end

Es la persona que tiene los conocimientos para el desarrollo del entorno de un sitio web; es decir interactua y conecta a los visitantes con la pagina web.

  

Tambien parte de su labor es comunicarse con el dearrolador Back-endy hacer consultas utiles para el sitio web.

  

### Tecnologías de desarrollo que un Front-End debe conocer

Las tecnologias que este debe manejar basicamente son: **HTML, CSS y Javascript**. Claro hay otra alternativas el dia de hoy para cumplir la funcion de cada uno de estos que hacen mas versatil el trabajo del Front-end

  

### Actividades de un Front-end

-  **Trabajar con el equipo de Diseño UX y el desarrollador back-end**

  

- **También debe tener habilidades en:**
  

1. *Diseño visual*

2. *Uso de herramientas de diseño visual* 

3. *Diseño de Experiencia de Usuarios*  

### Empresas que aportan al desarrollo Front-end

***Facebook*** y ***Google*** son dos grandes corporaciones que han aportado con el desarrollo de software como son: *React* y *Angular* respectivamente.

  

### ¿Cuál es el rol de un Front-end en las empresas del Perú?

En el país los desarrolladores de front end han desarrollado a la interface de usuario del publico consumidor, para que esta sea mas intuitiva y facil de usar para los clientes de paginas web de compras online, bancos y otros.

  
### ¿Cuál es el rol de un Front-end en las empresas del mundo?

Podemos observar estas en las mayores y mas conocidas aplicaciones de redes sociales del mundo, que realizan enfoques en la partes que el publico esta mas interesado como pueden ser fotos, videos, documentos, link de interes.

  
### Analicemos: ¿Qué es lo que más destaca en las siguientes páginas web?

![](imagen2.jpg)

 [ir a la pagina](https://interbank.pe/) 

![](imagen1.jpg)

[ir a la pagina](https://www.youtube.com/) 


### Conclusiones

**YOUTUBE**
  *Para ver múltiples opciones no hace
falta que la página recargue; sin
embargo, te muestra sutilmente cómo
va cargando.*

**INTERBANK**
    *La web es interactiva desde los
botones desplegables del menú y
productos, hasta el background del
formulario.*
  

### Tendencias visuales en el diseño web 2020

  

1.  *Elementos dinámicos*

2.  *Diseños asimétricos*

3.  *Efecto movimiento*

4.  *Diseños 3D*

5.  *Contenido dividido o fusionado*

6.  *Efectos hover*

7.  *Desplazamiento vertical*

8.  *Navegación oculta*

9.  *Barras adhesivas*

  

### Roadmap de un desarrollador Front-end

![](rutaFE.jpeg)
  
### Tendencias para Front-end 2020:

  

-  **PWA (Progressive Webs Apps)**, consumen otras tecnologías hasta API’s del navegador

para poder realizar determinadas funcionalidades, así como también puede seguir

ejecutándose en segundo plano sin necesidad de estar dentro del Navegador y muchas

otras más funcionalidades, es como hacerte ver que tu web es una Aplicación.

Ejemplo: https://hightide.earth/ es una aplicación que te permite buscar olas.

-  **Componentes en la nube**, frameworks como Vuejs, Reactjs y Angular, han impulsado el

uso de componentes, gracias a esto existe herramientas que nos permite subir nuestros

componentes dentro de una comunidad donde podríamos poder reutilizarlas luego.

Ejemplo: https://bit.dev/ nos permite subir nuestro components a la nube, para poder

compartirlos.

-  **Typescript**, a todos nos gusta tener un código limpio y fácil de entender, esto es por obvias

razones. Cuando se desarrolla un proyecto, es necesario aportar soluciones, correcciones

o cambios rápidamente y sin complicaciones, TypeScript ofrece esto y muchas otras

características más. Además, es compatible con los Frameworks más populares de la

actualidad. https://www.typescriptlang.org/.

-  **Módulos ECMA Script vía CDN**, es como tener lo mismo que con bit, pero esta vez por

CDN https://jspm.io/.

-  **Web Assembly**, imagínate que aplicaciones Desktop como MySQL Workbench, Illustrator

CC, etc. se puedan ejecutar desde el mismo navegador. Esta tecnología permite ejecutar el

código de un Proyecto en código binario, y por ende, de manera muy rápida

https://webassembly.org/.

  

## Un desarrollador Back-end

  Es la persona que estructura la data que el sitio web requiere para completar su funcionalidad, como son los formularios, listas, archivos de la base de datos, procesar información, servicios, etc.
  
Cabe resaltar que el trabajo del Back end no se aprecia visualmente en el sitio web.
 
### Tecnologías de desarrollo que un Back-End debe conocer
Entra las tecnologías que debe conocer un desarrollador Back-end están: 
- *Lenguajes de programación y frameworks* 
- *Base de datos* 
- *Servidores web* 
- *Protocolos SSL, HTTP, HTTPS*

### Lenguajes de programación y Frameworks

No existe un solo lenguaje de programación sino al contrario existen diferentes lenguajes que se pueden elegir como son:

- Python
- PHP
- ASP.Net
- etc

### Bases de datos
Se llama base de datos, o también _banco de datos_, a un **conjunto de  información  perteneciente a un mismo contexto**, ordenada de modo sistemático para su posterior recuperación, análisis y/o transmisión.  
  
Existen hoy varias opciones, **MySQL, Postgres, MongoDb, MariaDB, etc.** 

### Servidores web y protocolos
Un **servidor de red**, o simplemente **servidor**, es un ordenador o equipo informático que ofrece acceso a recursos y servicios compartidos a otros equipos conectados en red denominados clientes.

**Protocolo SSl**
SSL es el acrónimo de Secure Sockets Layer (capa de sockets seguros), la tecnología estándar para mantener segura una conexión a Internet, así como para proteger cualquier información confidencial que se envía entre dos sistemas e impedir que los delincuentes lean y modifiquen cualquier dato que se transfiera, incluida información que pudiera considerarse personal

**Protocolos de internet: HTTP / HTML / SMTP / POP**

*Los protocolos de internet*  permiten, entre otras cosas, que envíes correos electrónicos o puedas acceder a un sitio web. En conjunto, los protocolos de internet se implementan para poder gestionar de forma eficaz toda la información que circula constantemente por la red.

### Actividades de un Back-end

-   *Gestionar el desarrollo de funciones que simplifiquen el proceso de desarrollo.*
-   *Acciones de lógica.*
-   *Uso de librerías del servidor.*
-   *Conexión con bases de datos.*
-   *Configurar los servidores.*
-   *Diseñar soluciones para la ejecución.*
-   *Procesar información.*

### Conceptos que debe conocer un desarrollador Back-end

Algunas de las habilidades que un  **Desarrollador Backend** debe tener son:

- *Manejar al menos un CMS.*
-   *Saber de Metodologías de programación.*
-   *Conocer los fundamentos de la programación.*
-   *Ser muy lógico y ordenado.*
-   *Ser constante.*
-   *Alta capacidad de concentración.*

### Roadmap de un desarrollador Back-end

![](rutaBE.jpeg)

### Tendencias para Back-end 2020:

Lenguajes de programación y frameworks que debe dominar un  **Desarrollador Backend** :

-   PHP: Lenguaje de código abierto diseñado para el desarrollo web de contenido dinámico.    
-   Java: Orientado a objetos, su intención principal es permitir que los desarrolladores escriban un programa, y posteriormente puedan ejecutarlo en cualquier dispositivo.    
-   Javascript: Lenguaje orientado a objetos, más utilizado para páginas web.    
-   Python: Lenguaje de programación interpretado que pretende que la sintaxis siempre asegure que el código siempre sea legible.    
-   Ruby: Lenguaje de programación orientado a objetos que combina la sintaxis inspirada en Python y Perl.    
-   ASP.NET: plataforma de Microsoft para el desarrollo web.    
-   Node.js: Permite construir programas de red escalables de código abierto.
    

  

El  **desarrollador backend** debe estar familiarizado con bases de datos:

-   SQL Server    
-   MySQL    
-   Oracle    
-   PostgreSQL    
-   MongoDB    
-   NoSQL

### Analicemos: ¿Dónde crees que pueda estar el backend en las siguientes paginas?

**YOUTUBE**
En la casilla de búsqueda que nos muestra la acción mas parecida a nuestro historial de búsquedas, nos muestra nuestra cuenta de usuario asi como tambien los datos de esta misma, estos debieron de ser cargados de un servidor porporcionado por el back end.

**InterBanck**
cuando nos pide llenar algún formulario esta es contrastada con la base de datos para que valla apareciendo nuevas opciones en los combo box


### Conclusiones

- Podemos deducir que las tecnologías se van actualizando cada año, creciendo así el impacto dentro de nuestro mercado laboral.
- Tanto el back-end como el front-end son indispensable para proyectos de desarrollo web.