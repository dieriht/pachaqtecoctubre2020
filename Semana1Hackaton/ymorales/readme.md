# BACK END

**LOGRO:** Identificar los lenguajes de programación en tendencias dentro del mundo del Frontend y Backend, como también saber la diferencia entre ambos.

## Un desarrollador Front-end
Es la persona encargada de la parte visual, para que tenga un bonito diseño, adaptativo y facil de usar. Donde un usuario interactúa sin saber lo que pasa detras de cada acción que realiza(back-end).

![](imagen/frontback.jpg)

### - Tecnologías de desarrollo que un Front-End debe conocer

Las tecnologías principales que debe conocer un desarrollador Front-End son: **HTML, CSS, Sass, Javascript.**

Estas tecnologías tienen diversas metodologías y herramientas que ayudan a que el desarrollo sea mucho más fácil e interesante.

Como tendencias hoy en día existen frameworks como **Boostrap, Vuejs, Reactjs, etc.**

![](imagen/frontlenguajes.png)

### - Actividades de un Front-end

```mermaid
graph LR
A[Interactuar con el visual designer] -- UX Designer --> D{Frontend}
B[Convertir diseños web a HTML + CSS] --> D{Frontend}
C[Desarrollar interactividad y adaptibilidad <br />en una web] --> D{Frontend}
E[Desarrollar conexiones con servicios <br />para obtener contenido dinámico] --> D{Frontend}
```

### - Empresas que aportan al desarrollo Front-end

![](imagen/vuejs.jpg)
Es un **Framework progresivo**, es decir, es un Framework que sirve para consumir interfaz del usuario.

Fue creado por **Evan You** que trabajaba en **Google** realizando prototipos y en el core del Framework de Meteor, hasta que pensó en otra forma de hacer una opción más fácil que abarcara las necesidades a la hora de hacer prototipos. Así surgió **Vue** en el 2014, desde entonces ha tenido una gran evolución y sigue creciendo en su versión 2 cada vez más y  más.

### - ¿Cuál es el rol de un Front-end en las empresas del Perú?

![](imagen/bcp.png)

La interfaz del bcp ha cambiado con el tiempo haciéndola cada vez más simple y sencilla de usar.

Es el trabajo de un front-end hacer esta interfaz interactiva y que no requiera de hacer recargos de página cada vez que se navega en las opciones.

### - ¿Cuál es el rol de un Front-end en las empresas del mundo?

![](imagen/facebook.png)

Se encarga de hacer funcional la galería de fotos, que los post puedan visualizarse al ‘scrollear ́, entre otros. Todo esto sin recargar la página.

### - Analicemos: ¿Qué es lo que más destaca en las siguientes páginas web?
![](imagen/pagalo-pe.png)

https://pagalo.pe/

### - Conclusiones

![](imagen/pagalo-pe.png)

Fácil de usar, Intuitivo y se ve bien en diferentes plataformas.

### - Tendencias visuales en el diseño web 2020

- [x] Elementosgrandes
- [x] primeromóvil
- [x] unapágina
- [x] asimétricas

Más info: [https://www.maxcf.es/tendencias-diseno-web-2020/]()

### - Roadmap de un desarrollador Front-end

![](imagen/froadmap.png)

### - Tendencias para Front-end 2020:

- PWA (Progressive Webs Apps), consumen otras tecnologías hasta API’s del navegador para poder realizar determinadas funcionalidades, así como también puede seguir ejecutándose en segundo plano sin necesidad de estar dentro del Navegador y muchas otras más funcionalidades, es como hacerte ver que tu web es una Aplicación.
Ejemplo: https://hightide.earth/ es una aplicación que te permite buscar olas.
- Componentes en la nube, frameworks como Vuejs, Reactjs y Angular, han impulsado el uso de componentes, gracias a esto existe herramientas que nos permite subir nuestros componentes dentro de una comunidad donde podríamos poder reutilizarlas luego.
Ejemplo: https://bit.dev/ nos permite subir nuestro components a la nube, para poder compartirlos.
  
- Typescript, a todos nos gusta tener un código limpio y fácil de entender, esto es por obvias razones. Cuando se desarrolla un proyecto, es necesario aportar soluciones, correcciones o cambios rápidamente y sin complicaciones, TypeScript ofrece esto y muchas otras características más. Además, es compatible con los Frameworks más populares de la actualidad. https://www.typescriptlang.org/.
- Módulos ECMA Script vía CDN, es como tener lo mismo que con bit, pero esta vez por CDN https://jspm.io/.
- Web Assembly, imagínate que aplicaciones Desktop como MySQL Workbench, Illustrator CC, etc. se puedan ejecutar desde el mismo navegador. Esta tecnología permite ejecutar el código de un Proyecto en código binario, y por ende, de manera muy rápida https://webassembly.org/.

*CDN : Es básicamente un conjunto de servidores ubicados en diferentes puntos de una red que contienen copias locales de ciertos contenidos (vídeos, imágenes,
música, documentos, webs, etc.) que están almacenados en otros servidores, generalmente alejados geográficamente, de forma que sea posible servir dichos contenidos de manera más eficiente.*

## - Un desarrollador Back-end

Se encarga de la parte estructural y lógica de una solución digital y sirve para proveer de contenido solicitado por la parte de Front-end de una aplicación.

![](imagen/frontback.jpg)

*(como se ve en la figura, back-end es lo que no es visible para el usuario, el iceberg sería una página web como Twitter, Facebook, etc).*

![](imagen/backlogica.png)

Lógica de negocio

![](imagen/backseguridad.png)

Acceso y seguridad

![](imagen/backalmacena.png)

Almacena información

![](imagen/backservicios.png)

Provee servicios

![](imagen/backprocesa.png)

Procesa información

### - Tecnologías de desarrollo que un Back-End debe conocer

Entra las tecnologías que debe conocer un desarrollador Back-end están:

- [x]  Lenguajes de programación y frameworks
- [x]  Base de datos
- [x]  Servidores web
- [x]  Protocolos SSL, HTTP, HTTPS

![](imagen/backlenguajes.png)

### - Lenguajes de programación y Frameworks 

- [x] **Python:** fácil de aprender, veloz, semi-compilado y multipropósito. El framework para web más usado es Django.
- [x] **PHP:** por ejemplo, el famoso gestor de contenidos WordPress usa por detrás PHP. Laravel es uno de los frameworks usados con este lenguaje.
- [x] **Ruby:** es un lenguaje parecido al de Python, su framework más popular es Ruby on rails.
- [x] **Node.js:** se está haciendo cada vez más popular debido a que usa el mismo lenguaje que en el lado cliente: JavaScript, su framework para web más usado es Express.
- [x] **Java:** el lenguaje clásico y uno de los más demandados, su framework Spring. 
- [x] **ASP.NET:** es la plataforma de desarrollo web de Microsoft.

![](imagen/backdesarrollo.png)
 
 Más [https://www.ecured.cu/Lenguaje_de_programaci%C3%B3n_(inform%C3%A1tica)]()

### - Bases de datos

- [x] **MySQL:** base de datos relacional muy usada por millones de plataformas web actualmente es soportada por Oracle.
- [x] **Postgres:** base de datos relacional muy usada y de alto rendimiento y soporta base de datos geo espacial.
- [x] **MongoDB:** base de datos no relacional muy usada y conocida para registrar grandes cantidades de datos a un costo de tiempo y rendimiento menor.

![](imagen/backbd.png)
  
*Más [https://concepto.de/base-de-datos/]()*

### - Servidores web y protocolos

- [x] **Nginx:** es uno de los servidor web / proxy inverso ligero y de alto rendimiento y proxy para protocolos de correo como IMAP y POP3.
- [x] **Gunicorn:** es un servidor http que puede soportar desarrollos web en python con frameworks como django, flask entre otros.
- [x] **HTTPS** es un protocolo de aplicación basado en el protocolo HTTP, destinado a la transferencia segura de datos de Hipertexto, es decir, es la versión segura de HTTP.

![](imagen/backservidor.png)

Más [https://curiosoando.com/que-es-un-servidor-de-red[()]

### - Actividades de un Back-end

```mermaid
graph LR
A[Analizar y diseñar modelos de base datos] -- UX Designer --> D{Backend}
B[Coordinar con el desarrollador Front- end la comunicación de los servicios] --> D{Backend}
C[Desarrollar código que cumpla las condiciones del negocio] --> D{Backend}
E[Seguridad y despliegue de código fuente] --> D{Backend}
```
### - Conceptos que debe conocer un desarrollador Back-end

**Performance:** un desarrollador Back-end se enfrenta a diferentes retos, como por ejemplo, tener una respuesta rápida a sus servicios y que no se caiga por múltiples peticiones de los usuarios, como soportar la alta concurrencia de personas, por ejemplo, en el proceso de registro de un evento masivo.

El desarrollador Back-end puede abordar estos problemas con diferentes soluciones, como Memcache, desarrollo transaccional, desarrollar colas de mensajes, entre otras opciones.

![](imagen/backperformance.png)

**Infraestructura:** un desarrollador Back-end debe entender algunos conceptos de linux y poder usar un servidor en dicho sistema operativo, puesto que los grandes servidores del mundo usan Linux.
Tener clara la infraestructura que está montado el código del Back-end es una tarea también del desarrollador Back-end puesto que es importante conocer cuál es el rendimiento de la aplicación con el código hecho.

![](imagen/backinfra.png)

Más [http://www.databolivia.com/ws/index.php/extensions/s5-box]()

**Automatización:** una de las más grandes tareas del Back- end es desarrollar procesos automáticos para poder cargar información o migrar a otras bases de datos o hacer procesos en horarios nocturnos para centralizar y depurar información

**Ejemplo:**

Los bancos actualizan los morosos de sus tarjetas de crédito todos los días a las 4:05 am para luego ejecutar envíos masivos de correos o SMS a los celulares de los deudores.

![](imagen/backauto.png)

Más https://www.panel.es/zahori-automatizacion-de-pruebas/

### - Roadmap de un desarrollador Back-end

![](imagen/backroadmap.png)

### - Tendencias para Back-end 2020:

- Lenguajes populares :

![](imagen/backtendencias.png)

- Frameworks Populares :

![](imagen/backframe.png)

- Base de datos Populares :

![](imagen/backtopbd.png)

### - Analicemos: ¿Dónde crees que pueda estar el backend en las siguientes paginas? 

![](imagen/backyt.png)

www.youtube.com

![](imagen/backinterbank.png)

https://interbank.pe/

### - Conclusiones

![](imagen/backyt.png)

Al momento de poder observar los videos populares, mas vistos o también los que se acomodan a nuestra búsqueda diaria.

![](imagen/backinterbank.png)

Al momento de poder visualizar las promociones o también dentro de su banca segura.

- [x] Podemos deducir que las tecnologías se van actualizando cada año, creciendo así el impacto dentro de nuestro mercado laboral.
- [x] Tanto el back-end como el front-end son indispensable para proyectos de desarrollo web.