Edwar Quea
Sesion1 Clase1 "Hackaton"
# BACK END
## Un desarrollador Front-end
Un desarrollador **Front-end** se encarga de la parte visual de un sitio web o una aplicación. Esta en contacto permanente con los desarrolladores **Back-end**
![](frontend.jpg)
### Tecnologías de desarrollo que un Front-End debe conocer
  Las tecnologías principales que debe conocer un desarrollador **Front-End** son: **HTML, CSS y Javascript**.
  A su ves el desarrollador tiene que aprender a optimizar el tiempo automatizando el proceso de desarrollo. **Webpack** es una de las tecnologías. 
     Tambien existen mas tecnologias que pueden hacer mas eficiente el proceso de desarrollo.
![](tecnologiafrontend.jpg)
### Actividades de un Front-end
-Interactuar con el visual designer (UX Designer)
-Convertir diseños web a HTML+ CSS
-Desarrollar interactividad y adaptabilidad en una web
-Desarrollar conexiones con servicios para obtener contenido dinámico
-Programar, cambiar y mantener un sitio web.
### Empresas que aportan al desarrollo Front-end
Así como **FACEBOOK** , **GOOGLE**, que tiene un gran desarrollo en la parte visual de sus paginas otras empresas tambien le siguen los pasos como **INSTAGRAM**, **TIKTOK**, etc. La tendecia es al nivel de ofrecer al usuario una información y un sitio amigable.
![](facebook.jpg)
![](instagram.jpg)
### ¿Cuál es el rol de un Front-end en las empresas del Perú?
El tema de publicidad siempre fue ligado a anuncios comerciales, pero no lo es todo. el usuario pide mas, sus necesidades han crecido, la satisfacción del usuario es la prioridad. 
Como llegamos a la satisfacción del usuario peruano, haciendo amigable el entorno, que la información este al alcance de el, darle una herramienta fácil de utilizar.
Muchas empresas peruanas apuestan por esto: **Entidades Bancarias**, **Tiendas Online**
### ¿Cuál es el rol de un Front-end en las empresas del mundo?
Integrar al usuario con la informacion que desea.
-**Chats**
-**Pagos**
-**Fotos**
-**Videos**
-**Compras**
### Analicemos: ¿Qué es lo que más destaca en las siguientes páginas web?
- **NETFLIX**
- **FACEBOOK**
![](netflix.jpg)
![](facebook.jpg)
### Conclusiones
-**NETFLIX**. Para ver múltiples opciones no hace falta que la página recargue; sin embargo, te muestra sutilmente cómo va cargando.
- **FACEBOOK**. La información entre varios usuarios no se ve tan densa. el formato es amigable.
### Tendencias visuales en el diseño web 2020
-   [#1 Elementos dinámicos](https://www.maxcf.es/tendencias-diseno-web-2020/#1_Elementos_dinamicos)
-   [#2 Diseños asimétricos](https://www.maxcf.es/tendencias-diseno-web-2020/#2_Disenos_asimetricos)
-   [#3 Efecto movimiento](https://www.maxcf.es/tendencias-diseno-web-2020/#3_Efecto_movimiento)
-   [#4 Diseños 3D](https://www.maxcf.es/tendencias-diseno-web-2020/#4_Disenos_3D)
-   [#5 Contenido dividido o fusionado](https://www.maxcf.es/tendencias-diseno-web-2020/#5_Contenido_dividido_o_fusionado)
-   [#6 Efectos hover](https://www.maxcf.es/tendencias-diseno-web-2020/#6_Efectos_hover)
-   [#7 Desplazamiento vertical](https://www.maxcf.es/tendencias-diseno-web-2020/#7_Desplazamiento_vertical)
-   [#8 Navegación oculta](https://www.maxcf.es/tendencias-diseno-web-2020/#8_Navegacion_oculta)
-   [#9 Barras adhesivas](https://www.maxcf.es/tendencias-diseno-web-2020/#9_Barras_adhesivas)
### Roadmap de un desarrollador Front-end
![](roadmapfrontend.jpg)
### Tendencias para Front-end 2020:
- **PWA (Progressive Webs Apps)**, consumen otras tecnologías hasta API’s del navegador para poder realizar determinadas funcionalidades, así como también puede seguir ejecutándose en segundo plano sin necesidad de estar dentro del Navegador y muchas otras más funcionalidades, es como hacerte ver que tu web es una Aplicación. 
 - **Componentes en la nube**, frameworks como Vuejs, Reactjs y Angular, han impulsado el uso de componentes, gracias a esto existe herramientas que nos permite subir nuestros componentes dentro de una comunidad donde podríamos poder reutilizarlas luego.
- **Typescript**, a todos nos gusta tener un código limpio y fácil de entender, esto es por obvias razones. Cuando se desarrolla un proyecto, es necesario aportar soluciones, correcciones o cambios rápidamente y sin complicaciones, TypeScript ofrece esto y muchas otras características más. Además, es compatible con los Frameworks más populares de la actualidad. https://www.typescriptlang.org/. 
- **Módulos ECMA Script vía CDN**, es como tener lo mismo que con bit, pero esta vez por CDN https://jspm.io/. - Web Assembly, imagínate que aplicaciones Desktop como MySQL Workbench, Illustrator CC, etc. se puedan ejecutar desde el mismo navegador. Esta tecnología permite ejecutar el código de un Proyecto en código binario, y por ende, de manera muy rápida https://webassembly.org/.
  ## Un desarrollador Back-end
Se encarga de la parte estructural y lógica de una solución digital y sirve para proveer de contenido solicitado por la parte de Front-end de una aplicación. Es la parte que no ve el usuario.
![](frontend.jpg)
### Tecnologías de desarrollo que un Back-End debe conocer
Entra las tecnologías que debe conocer un desarrollador Back-end 
están: 
-  **Lenguajes de programación y frameworks**
-  **Base de datos** 
-  **Servidores web** 
-  **Protocolos SSL, HTTP, HTTPS**
-  **PHP**
![](tecnologiabackend.jpg)
### Lenguajes de programación y Frameworks
-**Python**: fácil de aprender, veloz, semi-compilado y multipropósito. El framework para web más usado es Django. 
-**PHP**: por ejemplo, el famoso gestor de contenidos WordPress usa por detrás PHP. Laravel es uno de los frameworks usados con este lenguaje. 
-**Ruby**: es un lenguaje parecido al de Python, su framework más popular es Ruby on rails. ]
-**Node.js**: se está haciendo cada vez más popular debido a que usa el mismo lenguaje que en el lado cliente: JavaScript, su framework para web más usado es Express. 
-**Java**: el lenguaje clásico y uno de los más demandados, su framework Spring. 
-**ASP.NET**: es la plataforma de desarrollo web de Microsoft
### Bases de datos
-**MySQL**: base de datos relacional muy usada por millones de plataformas web actualmente es soportada por Oracle.
-**Postgres**: base de datos relacional muy usada y de alto rendimiento y soporta base de datos geo espacial. 
-**MongoDB**: base de datos no relacional muy usada y conocida para registrar grandes cantidades de datos a un costo de tiempo y rendimiento menor.
### Servidores web y protocolos
-**Nginx**: es uno de los servidor web / proxy inverso ligero y de alto rendimiento y proxy para protocolos de correo como IMAP y POP3. 
-**Gunicorn**: es un servidor http que puede soportar desarrollos web en python con frameworks como django, flask entre otros. 
-**HTTPS**: es un protocolo de aplicación basado en el protocolo HTTP, destinado a la transferencia segura de datos de Hipertexto, es decir, es la versión segura de HTTP.
### Actividades de un Back-end
- Analizar y diseñar modelos de base datos
- Coordinar con el desarrollador Front-end la comunicación de los servicios
- Desarrollar código que cumpla las condiciones del negocio
- *Seguridad y despliegue de código fuente
### Conceptos que debe conocer un desarrollador Back-end
-**Performance**: un desarrollador **Back-end** se enfrenta a diferentes retos, como por ejemplo, tener una respuesta rápida a sus servicios y que no se caiga por múltiples peticiones de los usuarios, como soportar la alta concurrencia de personas, por ejemplo, en el proceso de registro de un evento masivo.
-**Infraestructura**: un desarrollador Back-end debe entender algunos conceptos de linux y poder usar un servidor en dicho sistema operativo, puesto que los grandes servidores del mundo usan Linux. Tener clara la infraestructura que está montado el código del Back-end es una tarea también del desarrollador Back-end puesto que es importante conocer cuál es el rendimiento de la aplicación con el código hecho
-**Automatización**: una de las más grandes tareas del Backend es desarrollar procesos automáticos para poder cargar información o migrar a otras bases de datos o hacer procesos en horarios nocturnos para centralizar y depurar información
### Roadmap de un desarrollador Back-end
![](roadmapbackend.jpg)
### Tendencias para Back-end 2020:
- Lenguajes populares 
- Frameworks Populares 
- Base de datos Populares 
### Analicemos: ¿Dónde crees que pueda estar el backend en las siguientes paginas?
- **NETFLIX**
- **FACEBOOK**
![](netflix.jpg)
![](facebook.jpg)
### Conclusiones
- **NETFLIX**. Al momento de poder observar los videos por categorias, mas vistos o también los que se acomodan a nuestra búsqueda.
- **FACEBOOK**.Al momento de poder visualizar noticias de otros usuarios en el mismo tiempo.